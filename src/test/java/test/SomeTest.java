package test;

import com.fitechsource.test.TestException;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class SomeTest {

    @Test
    public void syncBlock() throws TestException {
        Set<Double> res = new HashSet<>();
        com.fitechsource.test.Test.syncBlock(res);
        Assert.assertTrue(10 < res.size());
    }
}