package com.fitechsource.test;

import java.util.*;

/**
 * Should be improved to reduce calculation time.
 * <p>
 * Change it or create new one. (max threads count is com.fitechsource.test.TestConsts#MAX_THREADS)
 */
public class Test {
    public static void main(String[] args) throws TestException {

        Set<Double> res = new HashSet<>();
        long startTime = System.nanoTime();
        syncBlock(res);
        long endTime = System.nanoTime();
        System.out.println(res);

        double duration = (double) (endTime - startTime) / 1000000;
        System.out.println("Duration: " + duration);
    }

    public static void syncBlock(Set<Double> res) throws TestException {
        for (int i = 0; i < TestConsts.N; i++) {
            res.addAll(TestCalc.calculate(i));
        }
    }
}
